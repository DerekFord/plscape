﻿using PLScape.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace PLScape.WebApp.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
           if (Request.Form["adminLogin"] != null)
            {
                if (user._ID == 123 && user._Password == "admin")
                {
                    Admin admin = new Admin(user._ID, "Admin Name");
                    if (ModelState.IsValid)
                    {
                        
                        return RedirectToAction("AdminHome", "Admin");
                    }
                    else
                    {
                        // there is a validation error
                        return View();
                    }
                }
            }
            return View();
        }
    }
}