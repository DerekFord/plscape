﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PLScape.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        [HttpGet]
        public ActionResult AdminHome()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AdminAction()
        {
            if (ModelState.IsValid)
            {
                if (Request.Form["workScheduler"] != null)
                {
                    return View("CalendarView");
                }
            }

            return View("CalendarView");
        }

        public JsonResult GetEvents()
        {
            using (PLScapeDBEntities1 dbEntities = new PLScapeDBEntities1())
            {
                var events = dbEntities.Events.ToList();
                return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

            }
        }
        
        public ActionResult AddJob()
        {

            return View();
        }
        
        [HttpPost]
        public ActionResult AddJobInfo(Event e)
        {
            if(! e.Equals(null)) { 
                using(PLScapeDBEntities1 dbEntities = new PLScapeDBEntities1())
                {
                
                    dbEntities.Events.Add(e);
              
                    dbEntities.SaveChanges();
                }
            }

            return View("AddJob");
        }
    }
}