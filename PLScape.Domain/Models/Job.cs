﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLScape.Domain.Models
{
    public class Job
    {
        public int _ID { get; set; }

        public string _Description { get; set; }

        public string _Subject { get; set; }

        public DateTime _Start { get; set; }

        public DateTime _End { get; set; }

        //public string _Crew { get; set; }

        public string _ThemeColour { get; set; }

       // public string _Address { get; set; }

       // public string _JobNotes { get; set; }

        public bool _IsFullDay { get; set; }

        //Job name

    }
}
