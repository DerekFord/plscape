﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLScape.Domain.Models
{
    public class Admin : User
    {
        public Admin(int id, string name)
        {
            this._ID = id;
            this._Name = name;
        }

    }
}
