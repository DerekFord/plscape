﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLScape.Domain.Models
{
   public class User
    {
        [Required (ErrorMessage = "Please enter your ID")]
        public int _ID { get; set; }

        
        public string _Name { get; set; }

        [Required(ErrorMessage = "Please enter a password")]
        public string _Password { get; set; }
    }
}
